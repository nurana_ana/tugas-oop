<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$animal = new Animal("Shaun");
echo "Nama Animal : " . $animal->name . "<br>";
echo "Legs : " . $animal->legs . "<br>";
echo "cold_blooded : " . $animal->cold_blooded . "<br><br>";

$frog = new Frog("buduk");
echo "Nama Animal : " . $frog->name . "<br>";
echo "Legs : " . $frog->legs . "<br>";
echo "cold_blooded : " . $frog->cold_blooded . "<br>";
echo "jump : " . $frog->jump . "<br><br>";

$ape = new ape("Kera sakti");
echo "Nama Animal : " . $ape->name . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "cold_blooded : " . $ape->cold_blooded . "<br>";
echo "yell : " . $ape->yell . "<br><br>";

?>